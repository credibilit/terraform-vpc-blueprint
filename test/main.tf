module "test" {
  source                    = "../"

  name                      = "${local.name}"
  domain_name               = "${local.domain_name}"
  cidr_block                = "${local.vpc_cidr_block}"
  azs                       = "${data.aws_availability_zones.azs.names}"
  az_count                  = "${local.az_count}"
  hosted_zone_comment       = "${local.hosted_zone_comment}"
  public_subnets_cidr_block = "${local.subnets_cidr_blocks}"
  map_public_ip_on_launch   = "${local.map_public_ip_on_launch}"
  tags                      = "${local.tags}"
}

output "vpc" {
  value = "${module.test.vpc}"
}

output "private_route_tables" {
  value = "${module.test.private_route_tables}"
}

output "public_subnets" {
  value = "${module.test.public_subnets}"
}

output "dhcp_options" {
  value = "${module.test.dhcp_options}"
}

output "s3_endpoint" {
  value = "${module.test.s3_endpoint}"
}
