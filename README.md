AWS VPC Blueprint
============================

Creates a basic network with many core network resources on AWS, following best practices. Essencially this creates:

- A new Virtual Private Cloud (VPC).
- A new Route53 private zone, or associate a existing one with the VPC.
- A new DHCP options (paired with the private zone above), or associate a existing one with the VPC.
- An new VPC S3 endpoint with routes associations for all route tables.
- All resources which support tags have the Tag name created and is allow to set common tags to all resources.


# Sample use

To create the network with this module you need to insert the following peace of code on your own modules, there are lot of other parameters, check :

```
module "test" {
  source          = "git::https://bitbucket.org/credibilit/terraform-vpc-blueprint.git?ref=<VERSION>"

  name                      = "..."
  domain_name               = "..."
  cidr_block                = "..."
  hosted_zone_comment       = "..."
  tags                      = {...}
}
```

Where `<VERSION>` is the desired version of *this* module. The master branch store the list of versions which can be used. The possible parameters are listed in advance on this document.

## Diagram

![Alt text](https://bitbucket.org/credibilit/terraform-vpc-blueprint/raw/9a5c50d22b52335cb8fd205c1d7efe150d8abf19/docs/blueprint-vpc.jpg)

*source image is in folder /docs and It was create on draw.io*

## Compatibilities

The version 2.0.0 and above are created to be used with Terraform 0.11 (possibly its compatible with 0.9, but this was not tested). For version 0.8 use version 0.0.x or the latest version tagged from a commit from 0.8.x branch.

## Input Parameters

The following parameters are used on this module:

- `name`: A name for the VPC and related elements.
- `cidr_block`: The VPC network CIDR Block
- `instance_tenancy`: The VPC tenancy default when launching instances. Valid values are 'default' and 'dedicated' (default: default)
- `domain_name`: The internal DNS domain name for the VPC and DHCP options. required if at least one of them is enabled - `hosted_zone_comment`: A comment for the internal hosted zone. This is ignored if custom_route53_zone_id is set
- `custom_route53_zone_id`: The id of an existing route52 private zone to be associate with the VPC. This will mitigate the creation of a new zone.
- `custom_dhcp_options_id`: The ID of the DHCP Options to associate to the VPC. This will mitigate the creation of an DHCP option by this blueprint.
- `domain_name_servers`: The list of DNS servers address to put on resolv.conf (default: AmazonProvidedDNS).
- `ntp_servers`: A list of NTP servers to deliver via DHCP (default [])
- `netbios_name_servers`: A list of netbios name servers to deliver via DHCP (default [])
- `netbios_node_type`: The NetBIOS node type (1, 2, 4, or 8). AWS recommends to specify 2 since broadcast and multicast are not supported in their network. For more information about these node types, see RFC 2132. (default 2).
- `map_public_ip_on_launch`: If the public subnets should associate an public IP for instances launched on them. Use with caution (default false)
- `s3_endpoint_policy`: The endpoint policy JSON body (default empty)
- `tags`: A map with extra tags (up to 9) to be put in the resources. The Name tag is already handled by the blueprint and should not be referenced here (default {})

## Output parameters

This are the outputs exposed by this module.

- vpc:
    - id: id da VPC
    - cidr_block: VPC CIDR block.
    - cidr_block_bits: bits significativos para o CIDR.
    - private_zone: Route53 private zone id associated to VPC.
    - instance_tenancy: The default instance tenancy set for the VPC.
    - default_network_acl_id: The VPC default network ACL.
    - default_security_group_id: The default security groups for resources created without it.
    - default_route_table_id: The VPC route table to associate fresh created subnets.
    - network_address: The network classic address notation.
    - network_mask: The network classic mask notation.
- dhcp_options:
    - id: DHCP options id associated with the VPC.
    - domain_name_servers: configured name servers on DHCP options (only used if create a new one).
    - ntp_servers: configured time servers on DHCP options (only used if create a new one).
    - netbios_name_servers: configured netbions name servers on DHCP options (only used if create a new one).
    - netbios_node_type: configured netwbion name servers type on DHCP options (only used if create a new one).
- s3_endpoint:
    - id: The S3 VPC endpoint id.
    - prefix_list: The S3 VPC endpoint prefix list.
