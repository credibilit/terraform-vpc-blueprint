// Account ID for provider
variable account {}

// Static values
locals {
  name                    = "acme-test"
  domain_name             = "acme.cloud"
  vpc_cidr_block          = "10.0.0.0/16"
  subnets_cidr_blocks     = [
    "10.0.0.0/24",
    "10.0.1.0/24",
    "10.0.2.0/24",
    "10.0.3.0/24"
  ]
  az_count                = 4
  hosted_zone_comment     = "VPC internal DNS zone for tests"
  map_public_ip_on_launch = true
  tags                    = {
    Environment = "tst"
    Stack = "sample"
  }
}

// Region AZs
data "aws_availability_zones" "azs" {
  state = "available"
}
